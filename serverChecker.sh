#!/bin/bash
workDirectory=/home/ubuntu/project/parse-server
logFile=$workDirectory/devServerLog

PID=$(ps auxw | grep npm | grep -v grep | awk '{print $2}') > /dev/null
if [ "$PID" ]; then
    echo 'Server is good' $PID
else
    echo 'Restart Server'
    pm2 kill
    # clean port 1337
    PORT=$(fuser 1337/tcp) > /dev/null
    if [ "$PORT" ]; then
        echo 'kill 1337 port node (PID ' $PORT ')'
        kill $PORT
    fi
    # restart server
    cd $workDirectory
    echo 'Server is dead. Restart server by cronjob' >> $logFile
    npm start >> $logFile 2>&1
fi
