#!/bin/bash
workDirectory=/home/ubuntu/project/parse-server
logFile=$workDirectory/devServerLog

cd $workDirectory

git pull

#kill npm
PID=$(ps auxw | grep npm | grep -v grep | awk '{print $2}')
if [ "$PID" ]
then
    echo 'kill npm (PID' $PID ')'
    kill $PID
fi

#kill pm2
pm2 kill

#kill port
PORT=$(fuser 1337/tcp)
if [ "$PORT" ]
then
    echo 'kill 1337 port node (PID ' $PORT ')'
    kill $PORT
fi

# restart server
echo 'Update and restart server' >> $logFile
npm start 2>&1 | tee -a $logFile
